import { Injectable } from "@angular/core";
import { employee } from "./app.model";
import {HttpClient,HttpParams} from "@angular/common/http";

@Injectable({providedIn:'root'})
export class EmployeeService{
    constructor(private http:HttpClient){}
    employees:employee[]=[];
    ngOnInit(){
        console.log(" Entered Service");
        this.fetchEmployees();
    }
    fetchEmployees(){        
       return this.http.get<any>('http://localhost:8080/enrollees');
        
    }
    getEmployee(id:string){
        const getUrl='http://localhost:8080/enrollees/'+id;
        return this.http.get<any>(getUrl);
    }
    updateEmployee(emp:employee){
        const postUrl='http://localhost:8080/enrollees/'+emp.id;
        return this.http.put<any>(postUrl,emp);
    }

}