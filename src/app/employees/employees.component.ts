import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../employee.service';
import { employee } from '../app.model';
import { Router, ActivatedRoute} from '@angular/router';
@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit {

  employees:employee[]=[];
  constructor(private empServ: EmployeeService,private router:Router, private route:ActivatedRoute){}

  ngOnInit(): void {
    this.empServ.fetchEmployees().subscribe((data)=>{
      this.employees=data;
      //console.log(this.employees);
    });
  }
  editEmployeeDetails(emp: employee){
    this.router.navigate(['/employee',emp.id]);
  }

}
