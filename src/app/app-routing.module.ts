import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmployeesComponent } from "./employees/employees.component";
import { EditEmployeeComponent } from './edit-employee/edit-employee.component';

const routes: Routes = [
  {path:'',component:EmployeesComponent},
  {path:'employee/:id',component:EditEmployeeComponent}
 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
