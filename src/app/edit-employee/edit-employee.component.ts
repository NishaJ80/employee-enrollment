import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { EmployeeService } from '../employee.service';
import { employee } from '../app.model';

@Component({
  selector: 'app-edit-employee',
  templateUrl: './edit-employee.component.html',
  styleUrls: ['./edit-employee.component.css']
})
export class EditEmployeeComponent implements OnInit {

  constructor(private route:ActivatedRoute,private employeeService:EmployeeService,private router:Router) { }
  selectedEmployeeId:string='';
  selectedEmployee:employee={name:'',active:false};
  ngOnInit(): void {
    this.route.params.subscribe((params:Params)=>{
      this.selectedEmployeeId= params['id'];  
      console.log(this.selectedEmployeeId);
     });
      this.getSelectedEmployee();
      
    
  }
  getSelectedEmployee(){
    this.employeeService.getEmployee(this.selectedEmployeeId).subscribe((data)=>{
      this.selectedEmployee=data;
      console.log(this.selectedEmployee);
    });
  }
  onUpdateEmployee(){
    console.log(this.selectedEmployee);
    this.employeeService.updateEmployee(this.selectedEmployee).subscribe(responseData=>{
      console.log(responseData);
      this.router.navigate(['']);
    })
  }
}
